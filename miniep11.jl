function tratamentodastring(string)
    j = 1
    tamanho = 0
    x = zeros(length(string))
    for i = 1:length(string)
	valorchar = Int(string[i])
	if valorchar >= 65 && valorchar <= 90
	    x[j] = string[i]
	    j = j + 1
	end
        if valorchar >= 97 && valorchar <= 122
	    x[j] = string[i]
	    j = j + 1
	end
    end
    for i = 1:length(x)
        if x[i] != 0
	    tamanho = tamanho + 1
	    if x[i] >= 97
		x[i] = x[i] - 32
	    end
        end
    end
    y = zeros(tamanho)
    for i = 1:tamanho
	y[i] = x[i]
    end
    return y
end

function palindromo(string)
    iguais = true
    novastring = tratamentodastring(string)
    j = length(novastring) + 1
    for i = 1:length(novastring)
	if j >= i	    	
	    if novastring[i] == novastring[j - i]
	        iguais = true
	    else
	        iguais = false
	    end
	else
	    break
        end
     end
     return iguais
end

function testes()
    if palindromo("Passei em MAC0110!") == false && palindromo("Socorram-me, subi no ^onibus em Marrocos!") == true && palindromo("MiniEP11") == false && palindromo("ovo") && palindromo("") == true && palindromo("A m~ae te ama.") == true
	println("Passou nos testes! :)")
    else
	println("Falhou! :(")
    end
end

testes()
